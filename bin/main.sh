#!/usr/bin/env bash
set -euo pipefail

__dirname="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ $# -gt 0 ]; then
  cmd="${1}"
  shift
else
  cmd='--help'
fi

export opwd="$(pwd)"
cd "${__dirname}"

if [ -f "${__dirname}/${cmd}.sh" ]; then
  "./${cmd}.sh" "$@"
else
  export DOTDROP_PROFILE="${DOTDROP_PROFILE:-$(whoami)@$(hostname)}"
  ./dotdrop.sh "$cmd" "$@"
fi
  
