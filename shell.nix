{ pkgs ? import <nixpkgs> {} }:

# let
#   pythonWithPackages = pkgs.python3.withPackages (p: with p; [
#     pip
#   ]);
# 
# in
let
  pyPkgs = pkgs.python38Packages;
in pkgs.mkShell {
  buildInputs = with pyPkgs; [
    # pythonWithPackages
    # pkgs.python3.pkgs.requests
    pip
    python_magic
    setuptools
    wheel

    # keep this line if you use bash
    #pkgs.bashInteractive
  ];
  #shellHook = ''
    # Tells pip to put packages into $PIP_PREFIX instead of the usual locations.
    # export PIP_PREFIX=$(pwd)/_build/pip_packages
    # export PYTHONPATH="$PIP_PREFIX/${pkgs.python3.sitePackages}:$PYTHONPATH"
    # export PATH="$PIP_PREFIX/bin:$PATH"
    # unset SOURCE_DATE_EPOCH
  #'';  
  shellHook = ''
    export PIP_PREFIX=$(pwd)/_build/pip_packages
    export PYTHONPATH="$PIP_PREFIX/${pkgs.python38.sitePackages}:$PYTHONPATH"
    export PATH="$PIP_PREFIX/bin:$PATH"
    SOURCE_DATE_EPOCH=$(date +%s) pip install -r dotdrop/requirements.txt

    unset SOURCE_DATE_EPOCH
  '';
}
