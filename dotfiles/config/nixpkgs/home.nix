{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "micheal";
  home.homeDirectory = "/home/micheal";

  programs.fish = {
    enable = true;
    shellInit = ''
      # set -x --prepend NIX_PATH darwin-config=$HOME/.nixpkgs/darwin-configuration.nix:$HOME/.nix-defexpr/channels
      # fish_add_path ~/.local/bin ~/.nix-profile/bin /nix/var/nix/profiles/default/bin (nix-build '<darwin>' -A system --no-out-link)/sw/bin /opt/homebrew/bin /opt/homebrew/sbin
      fish_add_path ~/.local/bin ~/go/bin ~/.nix-profile/bin /nix/var/nix/profiles/default/bin

      [ -f ~/.asdf/asdf.fish ] && source ~/.asdf/asdf.fish
      direnv hook fish | source

      #set -x GPG_TTY (tty)
      #set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
      #gpgconf --launch gpg-agent
      fenv source ~/.profile

      # replace some builtins with alternatives
      alias cat=bat

    '';
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";

  fonts.fontconfig.enable = true;
  home.packages = with pkgs; [
    awscli2
    aws-vault
    aws-google-auth
    assh
    bat
    bpytop
    broot
    cookiecutter
    colorls
    curlie
    exa
    direnv
    delta
    dyff
    fd
    glow
    hub
    kubectl
    kubectx
    kubernetes-helm
    lorri
    lnav
    ncdu
    trash-cli
    tig
    silver-searcher
    simplescreenrecorder
    #shdotenv
    x11docker

    github-cli
    
    #wezterm

    keybase
    kbfs # keybase fuse
    keybase-gui

    yubikey-manager
    yubikey-agent
    yubikey-touch-detector
    #yubioath-desktop
    
    (nerdfonts.override { fonts = [ "FiraCode" "RobotoMono" ]; })
  ];

  services.keybase.enable = true;
  services.kbfs.enable = true;
  services.lorri.enable = true;

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

}
