local wezterm = require 'wezterm';

return {
  --- font stuff
  font = wezterm.font({
    "FiraCode Nerd Font Mono",
  }),

  harfbuzz_features = {
    "ss04", -- lighweight dollar sign
    "ss07", -- regexp match ligatures 
    "zero", -- dotted zero
    "onum", -- old-style figures
  },

  --- colors
  color_scheme = "MaterialDark",
  window_background_opacity = 0.75,

  --- other config
  check_for_updates = false,
  -- dpi = 24.0,
  enable_scroll_bar = false,

  --- tabs
  tab_bar_at_bottom = true,
  hide_tab_bar_if_only_one_tab = true;
}

