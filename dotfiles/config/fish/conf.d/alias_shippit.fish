set -gx AWS_VAULT_KEYCHAIN_NAME login
set -gx AWS_VAULT_PROMPT ykman

alias tools='aws-vault exec tools-readonly --'
alias dev='aws-vault exec dev-developer --'
