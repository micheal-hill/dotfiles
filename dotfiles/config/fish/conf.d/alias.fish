#alias la='colorls -Alh --gs --sort-files'
#alias ll='colorls -lh --gs --sort-files'
#alias ls='colorls --gs --sort-files'

alias la='exa --all --long --header --group-directories-first --group --icons --links --extended --git'
alias ll='exa       --long --header --group-directories-first --group --icons --links --extended --git'
alias ls='exa --group-directories-first --icons'

alias copy='xclip -sel clip'
alias ssh="assh wrapper ssh --"
alias nv=nvim

alias rm='echo "Use del or \\\\rm instead"; false'
alias del='trash-put'
alias using='aws-vault exec -s'

alias git='hub'

function cd
  functions -c cd _cd
  functions -e cd

  pushd $argv && colorls -lh --gs
  set s $status

  functions -c _cd cd
  functions -e _cd
  return $s
end

# some git stuff
alias pull='git pull'
alias push='git pull --rebase --autostash --all && git push'
