function _tide_item_aws
  switch "$AWS_VAULT"
    case ''
  	case "prod*" "prd*"
      set -g tide_aws_bg_color red
    case "stg*"
		  set -g tide_aws_bg_color brmagenta
  	case "*"
      set -g tide_aws_bg_color green
  end

  if set -q AWS_VAULT
    #set_color -b green
    #set -g tide_aws_bg_color green
    #set_color black
  	#printf ' %s' $AWS_VAULT
    #set -g tide_aws_bg_color green
    set -g tide_aws_color normal
    _tide_print_item aws ' ' $AWS_VAULT
  end
end

function __time_to_aws_expire
  math (date -d $AWS_SESSION_EXPIRATION +%s) - (date +%s)
end

function _tide_item_aws_time_to_expire
  if set -q AWS_SESSION_EXPIRATION
    set t_remains (__time_to_aws_expire)
    
    set -g tide_aws_time_to_expire_bg_color $tide_aws_bg_color
    #set_color blue
    _tide_print_item aws_time_to_expire '' (date -ud "@$t_remains" '+%T ' | tr -d '\n')
  end
end

function _refresh_aws_token --on-event runner:after
  if set -q AWS_VAULT; and test (__time_to_aws_expire) < 300
    echo "attempting to refresh AWS token..."

    set vault_profile $AWS_VAULT
    set -e AWS_VAULT

    exec aws-vault exec $vault_profile
  end
end
