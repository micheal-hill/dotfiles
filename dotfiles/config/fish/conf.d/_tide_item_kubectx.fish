function _tide_item_kubectx
  set -l ctx (kubectx -c 2>/dev/null)
  if [ ! -z "$ctx" ]
    set -g tide_kubectx_bg_color '#326ce5'
    set -g tide_kubectx_color black
    #set_color white
    #printf '%s' $ctx
    _tide_print_item kubectx '⎈ ' $ctx
  end
end
