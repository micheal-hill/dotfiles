[ -f ~/.asdf/asdf.fish ] && source ~/.asdf/asdf.fish
direnv hook fish | source
set PATH ~/.local/bin $PATH

set -x GPG_TTY (tty)
set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent