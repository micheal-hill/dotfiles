
function handle_bash_shell_hook --on-event fish_prompt
  if [[ -z shellHook ]]
    fenv (echo $shellHook | grep -o '^[^#]*' | grep export)
  end
end
