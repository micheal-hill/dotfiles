"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isExtLanguage = void 0;
const isExtLanguage = (s) => ["idris", "lidr", "markdown"].includes(s);
exports.isExtLanguage = isExtLanguage;
//# sourceMappingURL=languages.js.map