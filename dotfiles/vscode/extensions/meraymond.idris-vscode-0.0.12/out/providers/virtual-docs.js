"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.scheme = exports.provider = void 0;
const state_1 = require("../state");
/**
 * The content is generated previously when the command is run that opens the
 * virtual doc. This is necessary because that info is also required by the provider
 * that generates the semantic highlighting.
 */
exports.provider = {
    provideTextDocumentContent(uri) {
        const info = state_1.state.virtualDocState[uri.path];
        return info ? info.text : "";
    },
};
exports.scheme = "idris";
//# sourceMappingURL=virtual-docs.js.map