"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Provider = void 0;
const vscode = require("vscode");
const decorToKind = (decor) => {
    switch (decor) {
        case ":bound":
            return vscode.CompletionItemKind.Variable;
        case ":data":
            return vscode.CompletionItemKind.Enum;
        case ":function":
            return vscode.CompletionItemKind.Function;
        case ":keyword":
            return vscode.CompletionItemKind.Keyword;
        case ":metavar":
            return vscode.CompletionItemKind.Variable;
        case ":module":
            return vscode.CompletionItemKind.Module;
        case ":type":
            return vscode.CompletionItemKind.TypeParameter;
    }
};
class Provider {
    constructor(client) {
        this.client = client;
    }
    provideCompletionItems(document, position, _token, _context) {
        const range = document.getWordRangeAtPosition(position);
        const name = document.getText(range);
        return new Promise((res) => __awaiter(this, void 0, void 0, function* () {
            const reply = yield this.client.replCompletions(name);
            const completions = reply.completions.map((completion) => new vscode.CompletionItem(completion, vscode.CompletionItemKind.Function));
            res(completions);
        }));
    }
    resolveCompletionItem(item, _token) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            var _a, _b;
            const reply = yield this.client.docsFor(item.label, ":overview");
            if (reply.ok) {
                item.documentation = reply.docs;
                // The first line of the docs is the identifier itself, so the first
                // item of metadata is the type.
                const decor = ((_b = (_a = reply.metadata[0]) === null || _a === void 0 ? void 0 : _a.metadata) === null || _b === void 0 ? void 0 : _b.decor) || ":function";
                item.kind = decorToKind(decor);
            }
            resolve(item);
        }));
    }
}
exports.Provider = Provider;
//# sourceMappingURL=completions.js.map