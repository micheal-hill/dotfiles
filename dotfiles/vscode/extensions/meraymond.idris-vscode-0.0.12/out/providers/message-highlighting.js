"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.selector = exports.provider = exports.legend = exports.metadataToTokens = void 0;
const vscode = require("vscode");
const virtual_docs_1 = require("./virtual-docs");
const state_1 = require("../state");
const flattenToken = (token) => [
    token.deltaLine,
    token.deltaStart,
    token.length,
    token.tokenType,
    token.tokenModifiers,
];
const tokenTypes = ["enum", "function", "macro", "namespace", "type", "variable", "variable.readonly"];
const tokenModifiers = ["declaration"];
const decorToSelectorIndex = {
    ":bound": tokenTypes.indexOf("variable"),
    ":data": tokenTypes.indexOf("enum"),
    ":function": tokenTypes.indexOf("function"),
    ":keyword": tokenTypes.indexOf("variable.readonly"),
    ":module": tokenTypes.indexOf("namespace"),
    ":metavar": tokenTypes.indexOf("macro"),
    ":type": tokenTypes.indexOf("type"),
};
/**
 * Idris labels message metadata by its position relative to the start of the
 * string, treating new lines as any other char. VS labels its tokens relative
 * to the previous token, by line and char. This function converts from the
 * Idris system to VS.
 */
const metadataToTokens = (text, metadata) => {
    let tokenData = [];
    let pos = 0;
    let line = 0;
    let char = 0;
    let lastTokenLine = line;
    let lastTokenStart = char;
    metadata.forEach((m) => {
        /// walk forward until start of meta token
        while (pos < m.start) {
            if (text[pos] === "\n") {
                line += 1;
                char = 0;
            }
            else {
                char += 1;
            }
            pos += 1;
        }
        /// save this token
        const deltaLine = line - lastTokenLine;
        const deltaStart = line === lastTokenLine ? char - lastTokenStart : char;
        const token = {
            deltaLine,
            deltaStart,
            length: m.length,
            tokenType: decorToSelectorIndex[m.metadata.decor || ":bound"],
            tokenModifiers: 0,
        };
        tokenData = tokenData.concat(flattenToken(token));
        /// update last token state
        lastTokenLine = line;
        lastTokenStart = char;
        /// walk through meta token
        while (pos < m.start + m.length) {
            if (text[pos] === "\n") {
                // VS doesn’t accept multi-line tokens. I’m not sure if Idris will ever return one.
                throw "Blast and damn, these aren’t supposed to cross lines.";
            }
            else {
                char += 1;
            }
            pos += 1;
        }
    });
    return new Uint32Array(tokenData);
};
exports.metadataToTokens = metadataToTokens;
exports.legend = new vscode.SemanticTokensLegend(tokenTypes, tokenModifiers);
exports.provider = {
    provideDocumentSemanticTokens(document) {
        const info = state_1.state.virtualDocState[document.uri.path];
        return new Promise((resolve) => {
            if (info) {
                const { text, metadata } = info;
                const tokenData = exports.metadataToTokens(text, metadata);
                resolve(new vscode.SemanticTokens(tokenData));
            }
            else
                resolve(new vscode.SemanticTokens(new Uint32Array()));
        });
    },
};
/**
 * Match documents with the Virtual Document scheme.
 */
exports.selector = { scheme: virtual_docs_1.scheme };
//# sourceMappingURL=message-highlighting.js.map