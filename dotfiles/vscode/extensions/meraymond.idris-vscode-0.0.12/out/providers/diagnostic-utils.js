"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rmLocDesc = void 0;
const filenameRegex = /.*:\d+:\d+--\d+:\d+\n(.|\n)*?(\n\n|\n$)/;
const parseLineRegex = /Parse error at line \d+:\d+:\n/;
const rmFileSec = (warning) => warning.replace(filenameRegex, "");
const rmParseLine = (warning) => warning.replace(parseLineRegex, "");
const rmLocDesc = (warning) => rmFileSec(rmParseLine(warning)).trim();
exports.rmLocDesc = rmLocDesc;
//# sourceMappingURL=diagnostic-utils.js.map