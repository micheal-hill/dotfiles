"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleWarning = void 0;
const path_1 = require("path");
const vscode = require("vscode");
const state_1 = require("../state");
const diagnostic_utils_1 = require("./diagnostic-utils");
const warningToDiagnostic = (reply) => {
    const { start, end, warning, filename } = reply.err;
    const isLidr = path_1.extname(filename) === ".lidr";
    // .lidr positions are returned as if they’re normal Idris files, so they’re
    // off by two, for the `> ` at the beginning.
    const range = isLidr
        ? new vscode.Range(new vscode.Position(start.line - 1, start.column + 1), new vscode.Position(end.line - 1, end.column + 2))
        : new vscode.Range(new vscode.Position(start.line - 1, start.column - 1), new vscode.Position(end.line - 1, end.column));
    const editedWarning = diagnostic_utils_1.rmLocDesc(warning);
    return new vscode.Diagnostic(range, editedWarning);
};
const handleWarning = (reply) => {
    const { diagnostics, idrisProcDir } = state_1.state;
    const filename = reply.err.filename;
    // Idris2 sometimes uses relative file paths, which aren’t parsed into file URIs correctly on their own.
    const uri = path_1.isAbsolute(filename) ? vscode.Uri.file(filename) : vscode.Uri.file(idrisProcDir + "/" + filename);
    const existing = diagnostics.get(uri) || [];
    diagnostics.set(uri, existing.concat(warningToDiagnostic(reply)));
};
exports.handleWarning = handleWarning;
//# sourceMappingURL=diagnostics.js.map