"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Provider = void 0;
const vscode = require("vscode");
const state_1 = require("../state");
const commands_1 = require("../commands");
const doc_state_parser_1 = require("./doc-state-parser");
const languages_1 = require("../languages");
const lidrLineIsCode = (document, line) => document.lineAt(line).text.trim().startsWith(">");
const openingMarkdownBlock = (line) => /^(```|~~~)idris/.test(line.trim());
const closingMarkdownBlock = (line) => /^(```|~~~)\s*$/.test(line.trim());
const getStateWithinBlock = (document, position, startLine, endLine) => {
    const blockStart = new vscode.Position(startLine, 0);
    const blockEnd = new vscode.Position(endLine, 0);
    const block = new vscode.Range(blockStart, blockEnd);
    const relativePos = new vscode.Position(position.line - startLine, position.character);
    const parser = new doc_state_parser_1.DocStateParser(document.getText(block), relativePos);
    return parser.parseToEndPos();
};
/**
 * The, perhaps imperfectly named, DocStateParser determines whether the cursor is
 * over code within a source file. For literate Idris and markdown files, we first
 * need to determine if we’re within a code block at all. If so, we pass just that
 * code block to the DocStateParser, to see if within that block the cursor is
 * pointing at code.
 */
const overCode = (document, position) => {
    const lang = document.languageId;
    if (!languages_1.isExtLanguage(lang))
        return false;
    switch (lang) {
        case "idris": {
            const parser = new doc_state_parser_1.DocStateParser(document.getText(), position);
            const docStateAtPos = parser.parseToEndPos();
            return docStateAtPos === "code";
        }
        case "lidr": {
            const inCodeBlock = lidrLineIsCode(document, position.line);
            if (!inCodeBlock)
                return false;
            // Run the DocStateParser on just the code block that the hover position is within.
            let blockStartLine = position.line;
            let blockEndLine = position.line;
            while (lidrLineIsCode(document, blockStartLine) && blockStartLine > 0)
                blockStartLine--;
            while (lidrLineIsCode(document, blockEndLine) && blockEndLine <= document.lineCount)
                blockEndLine++;
            return getStateWithinBlock(document, position, blockStartLine, blockEndLine + 1) === "code";
        }
        case "markdown": {
            const hoverLineText = document.lineAt(position.line).text;
            // If we’re over a markdown code block delimeter, it’s not Idris code.
            if (openingMarkdownBlock(hoverLineText) || closingMarkdownBlock(hoverLineText))
                return false;
            // Look for opening of Idris code block.
            let blockStartLine = position.line;
            while (blockStartLine > 0) {
                blockStartLine--;
                const lineText = document.lineAt(blockStartLine).text;
                if (openingMarkdownBlock(lineText))
                    break;
                if (closingMarkdownBlock(lineText))
                    return false; // we’re not in a code block
            }
            if (blockStartLine === 0)
                return false; // we’ve reached the start without finding the opening of a code block
            // If we’ve gotten this far, we’re definitely within a code block.
            let blockEndLine = position.line;
            while (blockEndLine <= document.lineCount) {
                blockEndLine++;
                const lineText = document.lineAt(blockStartLine).text;
                if (closingMarkdownBlock(lineText))
                    break;
            }
            return getStateWithinBlock(document, position, blockStartLine + 1, blockEndLine) === "code";
        }
    }
};
const typeOf = (client) => (document, position) => new Promise((res) => __awaiter(void 0, void 0, void 0, function* () {
    const range = document.getWordRangeAtPosition(position);
    if (!range)
        return res(null);
    if (!overCode(document, position))
        return res(null);
    const name = document.getText(range);
    const trimmed = name.startsWith("?") ? name.slice(1, name.length) : name;
    const reply = yield client.typeOf(trimmed);
    res(reply.ok ? reply.typeOf : null);
}));
const typeAt = (client) => (document, position) => new Promise((res) => __awaiter(void 0, void 0, void 0, function* () {
    const range = document.getWordRangeAtPosition(position);
    if (!range)
        return res(null);
    if (!overCode(document, position))
        return res(null);
    const name = document.getText(range);
    const trimmed = name.startsWith("?") ? name.slice(1, name.length) : name;
    const reply = yield client.typeAt(trimmed, position.line + 1, position.character + 1);
    res(reply.ok ? reply.typeAt : null);
}));
class Provider {
    constructor(client) {
        this.typeOf = typeOf(client);
        this.typeAt = typeAt(client);
    }
    provideHover(document, position, _token) {
        switch (state_1.state.hoverAction) {
            case "Nothing":
                return null;
            case "Type Of":
                return this.typeOf(document, position).then((type) => type ? { contents: [{ value: type, language: "idris" }] } : null);
            case "Type At":
                if (!state_1.state.idris2Mode) {
                    commands_1.v2Only("Type At");
                    return null;
                }
                return this.typeAt(document, position).then((type) => type ? { contents: [{ value: type, language: "idris" }] } : null);
        }
    }
}
exports.Provider = Provider;
//# sourceMappingURL=hover.js.map