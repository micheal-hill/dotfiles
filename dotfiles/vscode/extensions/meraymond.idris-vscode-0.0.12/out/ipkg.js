"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.extractPkgs = void 0;
const extractPkgs = (fileContents) => {
    const pkgsMatches = fileContents.match(/pkgs\s*=\s*(([a-zA-Z/0-9., -_]+\s{0,1})*)/);
    const pkgs = pkgsMatches ? pkgsMatches[1].split(",").map((s) => s.trim()) : [];
    return pkgs;
};
exports.extractPkgs = extractPkgs;
//# sourceMappingURL=ipkg.js.map