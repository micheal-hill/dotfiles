"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.replaceRange = exports.replaceLine = exports.insertLine = exports.getIndent = exports.prevEmptyLine = exports.lineAfterDecl = exports.currentSelection = exports.currentWord = void 0;
const vscode = require("vscode");
const previousChar = (document, range) => document.getText(new vscode.Range(new vscode.Position(range.start.line, range.start.character - 1), new vscode.Position(range.start.line, range.start.character)));
/**
 * Get the term under the cursor.
 */
const currentWord = () => {
    const editor = vscode.window.activeTextEditor;
    const document = editor === null || editor === void 0 ? void 0 : editor.document;
    const position = editor === null || editor === void 0 ? void 0 : editor.selection.active;
    if (!position || !document)
        return null;
    const range = document === null || document === void 0 ? void 0 : document.getWordRangeAtPosition(position);
    if (!range)
        return null;
    // If the selection is embedded within a Markdown file, the word boundaries
    // won’t include preceding ?s, which is a problem when selecting holes.
    if (document.languageId === "markdown" && previousChar(document, range) === "?") {
        const extendedRange = range.with(range.start.with(range.start.line, range.start.character - 1), range.end);
        const name = document.getText(extendedRange);
        return { name, line: position.line, range: extendedRange };
    }
    const name = document.getText(range);
    return { name, line: position.line, range };
};
exports.currentWord = currentWord;
/**
 * Get the first term currently highlighted.
 */
const currentSelection = () => {
    const editor = vscode.window.activeTextEditor;
    const document = editor === null || editor === void 0 ? void 0 : editor.document;
    const selection = editor === null || editor === void 0 ? void 0 : editor.selection;
    if (document && (selection === null || selection === void 0 ? void 0 : selection.start) && (selection === null || selection === void 0 ? void 0 : selection.end)) {
        const range = new vscode.Range(selection === null || selection === void 0 ? void 0 : selection.start, selection === null || selection === void 0 ? void 0 : selection.end);
        const name = document.getText(range);
        return { name, line: selection.start.line, range };
    }
    else
        return null;
};
exports.currentSelection = currentSelection;
/**
 * Get the line after the current declaration. Naïve, doesn’t have any
 * knowledge of the AST, just skips over anything that’s indented.
 */
const lineAfterDecl = (declLine) => {
    const defaultTo = declLine + 1;
    const editor = vscode.window.activeTextEditor;
    const document = editor === null || editor === void 0 ? void 0 : editor.document;
    if (!document)
        return defaultTo;
    let insertAtLine;
    for (let nextLineNumber = declLine + 1; nextLineNumber < document.lineCount + 1; nextLineNumber++) {
        if (nextLineNumber === document.lineCount) {
            insertAtLine = nextLineNumber;
            break;
        }
        const nextLine = document.lineAt(nextLineNumber);
        const hasIndentedText = /^\s+\S+/.test(nextLine.text);
        if (!hasIndentedText) {
            insertAtLine = nextLineNumber;
            break;
        }
    }
    return insertAtLine || defaultTo;
};
exports.lineAfterDecl = lineAfterDecl;
/**
 * Get the first empty line above the current line.
 */
const prevEmptyLine = (fromLine, languageId) => {
    const defaultTo = fromLine - 1;
    const editor = vscode.window.activeTextEditor;
    const document = editor === null || editor === void 0 ? void 0 : editor.document;
    if (!document)
        return defaultTo;
    let insertAtLine;
    for (let line = fromLine - 1; line > 0; line--) {
        const prevLine = document.lineAt(line);
        const isEmptyLine = languageId === "lidr" ? /^>\s*$/.test(prevLine.text) : prevLine.isEmptyOrWhitespace;
        if (isEmptyLine) {
            insertAtLine = line;
            break;
        }
    }
    return insertAtLine || defaultTo;
};
exports.prevEmptyLine = prevEmptyLine;
/**
 * Get the indentation of a line.
 */
const getIndent = (line) => {
    const editor = vscode.window.activeTextEditor;
    const document = editor === null || editor === void 0 ? void 0 : editor.document;
    if (editor && document) {
        const l = document.lineAt(line);
        const end = l.firstNonWhitespaceCharacterIndex;
        return l.text.slice(0, end);
    }
    else
        return "";
};
exports.getIndent = getIndent;
/**
 * Insert `text` at the specified line and number, along with a new line.
 */
const insertLine = (text, line, column = 0) => {
    const editor = vscode.window.activeTextEditor;
    editor === null || editor === void 0 ? void 0 : editor.edit((eb) => {
        const pos = new vscode.Position(line, column);
        // If you try to insert past the end of the file, it will simply append it
        // to the final line, so in that case start with a newline.
        const prefix = line === editor.document.lineCount ? "\n" : "";
        const suffix = "\n";
        eb.insert(pos, prefix + text + suffix);
    });
};
exports.insertLine = insertLine;
/**
 * Replace the contents of the specified line with `text`.
 */
const replaceLine = (text, line) => {
    const editor = vscode.window.activeTextEditor;
    const document = editor === null || editor === void 0 ? void 0 : editor.document;
    if (editor && document) {
        editor.edit((eb) => {
            eb.replace(document.lineAt(line).range, text);
        });
    }
};
exports.replaceLine = replaceLine;
/**
 * Replace a range of text with the specified string.
 */
const replaceRange = (text, range) => {
    const editor = vscode.window.activeTextEditor;
    editor === null || editor === void 0 ? void 0 : editor.edit((eb) => {
        eb.replace(range, text);
    });
};
exports.replaceRange = replaceRange;
//# sourceMappingURL=editing.js.map