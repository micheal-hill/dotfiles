"use strict";
// This is a little hacky, but the Memento API is too clunky.
Object.defineProperty(exports, "__esModule", { value: true });
exports.diagnostics = exports.virtualDocState = void 0;
const vscode = require("vscode");
exports.virtualDocState = {};
exports.diagnostics = vscode.languages.createDiagnosticCollection("Idris Errors");
//# sourceMappingURL=global-state.js.map