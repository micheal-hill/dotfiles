"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.supportedLanguages = exports.initialiseState = exports.state = void 0;
const child_process_1 = require("child_process");
const idris_ide_client_1 = require("idris-ide-client");
const vscode = require("vscode");
const diagnostics_1 = require("./providers/diagnostics");
const ipkg_1 = require("./ipkg");
exports.state = {
    autosave: "always",
    client: null,
    currentFile: "",
    diagnostics: vscode.languages.createDiagnosticCollection("Idris Errors"),
    hoverAction: "Type Of",
    idrisProc: null,
    idrisProcDir: null,
    idris2Mode: false,
    outputChannel: vscode.window.createOutputChannel("Idris"),
    statusMessage: null,
    virtualDocState: {},
};
const replyCallback = (reply) => {
    switch (reply.type) {
        case ":warning":
            return diagnostics_1.handleWarning(reply);
        default:
    }
};
const initialiseState = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const extensionConfig = vscode.workspace.getConfiguration("idris");
    const idrisPath = extensionConfig.get("idrisPath") || "";
    const idris2Mode = extensionConfig.get("idris2Mode") || false;
    const autosave = extensionConfig.get("autosave");
    const hoverAction = extensionConfig.get("hoverAction");
    const workspacePaths = (_a = vscode.workspace.workspaceFolders) === null || _a === void 0 ? void 0 : _a.map((folder) => folder.uri.path);
    let idrisProcDir = null;
    if (idris2Mode && (workspacePaths === null || workspacePaths === void 0 ? void 0 : workspacePaths.length) === 1) {
        idrisProcDir = workspacePaths[0];
    }
    else {
        vscode.window.showErrorMessage("Multiple workspaces are not currently supported, and most features may not work correctly.");
    }
    /* Idris2 won’t locate the ipkg file by default if the code is in another
    directory, so it’s necessary to pass the --find-ipkg flag. It looks for the ipkg
    in parent directories of the process, so it’s also necessary to start the Idris
    process in the workspace directory.*/
    const procArgs = idris2Mode ? ["--ide-mode", "--find-ipkg", "--no-color"] : ["--ide-mode"];
    if (!idris2Mode) {
        const ipkgUries = yield vscode.workspace.findFiles("*.ipkg");
        if (ipkgUries.length > 0) {
            const ipkgFile = yield readFile(ipkgUries[0]);
            const pkgs = ipkg_1.extractPkgs(ipkgFile);
            pkgs.forEach((pkg) => procArgs.push("-p", pkg));
        }
    }
    const procOpts = idrisProcDir ? { cwd: idrisProcDir } : {};
    const idrisProc = child_process_1.spawn(idrisPath, procArgs, procOpts);
    idrisProc.on("error", (_) => {
        vscode.window.showErrorMessage("Could not start Idris process with: " + idrisPath);
    });
    if (!(idrisProc.stdin && idrisProc.stdout)) {
        throw "Failed to start Idris process."; // unreachable
    }
    const client = new idris_ide_client_1.IdrisClient(idrisProc.stdin, idrisProc.stdout, {
        debug: false,
        replyCallback,
    });
    exports.state.client = client;
    exports.state.idrisProc = idrisProc;
    exports.state.idrisProcDir = idrisProcDir;
    exports.state.idris2Mode = idris2Mode;
    if (autosave)
        exports.state.autosave = autosave;
    if (hoverAction)
        exports.state.hoverAction = hoverAction;
});
exports.initialiseState = initialiseState;
const supportedLanguages = (state) => state.idris2Mode ? ["idris", "lidr", "markdown"] : ["idris", "lidr"];
exports.supportedLanguages = supportedLanguages;
const readFile = (uri) => __awaiter(void 0, void 0, void 0, function* () {
    const readData = yield vscode.workspace.fs.readFile(uri);
    const data = Buffer.from(readData).toString("utf8");
    return data;
});
//# sourceMappingURL=state.js.map