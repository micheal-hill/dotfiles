"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
const vscode = require("vscode");
const commands_1 = require("./commands");
const completions = require("./providers/completions");
const hover = require("./providers/hover");
const messageHighlighting = require("./providers/message-highlighting");
const virtualDocs = require("./providers/virtual-docs");
const state_1 = require("./state");
const promptReload = () => {
    const action = "Reload Now";
    vscode.window
        .showInformationMessage("Reload window in order for configuration changes to take effect.", action)
        .then((selectedAction) => {
        if (selectedAction === action) {
            vscode.commands.executeCommand("workbench.action.reloadWindow");
        }
    });
};
const activate = (context) => __awaiter(void 0, void 0, void 0, function* () {
    yield state_1.initialiseState();
    const { client, diagnostics, virtualDocState } = state_1.state;
    if (client === null) {
        throw "Client should have been initialised by this point.";
    }
    // Activate the provider for all supported language ids.
    const selectSupported = state_1.supportedLanguages(state_1.state).map((language) => ({ language }));
    vscode.workspace.onDidChangeConfiguration((changeEvent) => {
        if (changeEvent.affectsConfiguration("idris.autosave")) {
            const autosave = vscode.workspace.getConfiguration("idris").get("autosave");
            if (autosave)
                state_1.state.autosave = autosave;
        }
        if (changeEvent.affectsConfiguration("idris.hoverAction")) {
            const hoverAction = vscode.workspace.getConfiguration("idris").get("hoverAction");
            if (hoverAction)
                state_1.state.hoverAction = hoverAction;
        }
        const procConfigChanged = changeEvent.affectsConfiguration("idris.idrisPath") || changeEvent.affectsConfiguration("idris.idris2Mode");
        if (procConfigChanged) {
            promptReload();
        }
    });
    vscode.workspace.registerTextDocumentContentProvider(virtualDocs.scheme, virtualDocs.provider);
    vscode.languages.registerDocumentSemanticTokensProvider(messageHighlighting.selector, messageHighlighting.provider, messageHighlighting.legend);
    vscode.languages.registerCompletionItemProvider(selectSupported, new completions.Provider(client));
    vscode.languages.registerHoverProvider(selectSupported, new hover.Provider(client));
    vscode.window.visibleTextEditors.forEach((editor) => commands_1.loadFile(client, editor.document));
    /* Hooks */
    const syncFileInfo = (document) => {
        var _a;
        // Clear old state
        diagnostics.delete(document.uri);
        (_a = vscode.window.activeTextEditor) === null || _a === void 0 ? void 0 : _a.setDecorations(commands_1.evalResult, []);
        // Get new state
        commands_1.loadFile(client, document);
    };
    vscode.workspace.onDidOpenTextDocument(syncFileInfo);
    vscode.workspace.onDidSaveTextDocument(syncFileInfo);
    vscode.workspace.onDidCloseTextDocument((document) => {
        diagnostics.delete(document.uri);
        delete virtualDocState[document.uri.path];
    });
    /* Commands */
    context.subscriptions.push(vscode.commands.registerCommand("idris.activate", () => {
        if (vscode.window.activeTextEditor)
            syncFileInfo(vscode.window.activeTextEditor.document);
    }));
    context.subscriptions.push(vscode.commands.registerCommand("idris.addClause", commands_1.addClause(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.addMissing", commands_1.addMissing(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.apropos", commands_1.apropos(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.aproposSelection", commands_1.aproposSelection(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.browseNamespace", commands_1.browseNamespace(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.caseSplit", commands_1.caseSplit(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.docsFor", commands_1.docsFor(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.docsForSelection", commands_1.docsForSelection(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.generateDef", commands_1.generateDef(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.interpretSelection", commands_1.interpretSelection(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.metavariables", commands_1.metavariables(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.printDefinition", commands_1.printDefinition(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.printDefinitionSelection", commands_1.printDefinitionSelection(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.makeCase", commands_1.makeCase(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.makeLemma", commands_1.makeLemma(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.makeWith", commands_1.makeWith(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.proofSearch", commands_1.proofSearch(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.typeAt", commands_1.typeAt(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.typeOf", commands_1.typeOf(client)));
    context.subscriptions.push(vscode.commands.registerCommand("idris.version", commands_1.version(client)));
});
exports.activate = activate;
const deactivate = () => {
    var _a;
    (_a = state_1.state.idrisProc) === null || _a === void 0 ? void 0 : _a.kill();
};
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map