"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.testCache = void 0;
class TestCache {
    constructor() {
        this.metadata = new WeakMap();
        this.suiteResults = new Map();
    }
    setMetadata(test, data) {
        this.metadata.set(test, data);
    }
    getMetadata(test) {
        return this.metadata.get(test);
    }
    /**
     * Get all descendant suites of a given TestItem.
     * For build target it returns all test suite within it, same for package.
     * If is called with TestItem which is a suite, then returns itself
     */
    getTestItemChildren(test) {
        var _a;
        if (((_a = this.getMetadata(test)) === null || _a === void 0 ? void 0 : _a.kind) === "suite") {
            return [test];
        }
        else {
            let children = [];
            test.children.forEach((child) => {
                const descendants = this.getTestItemChildren(child);
                children = [...children, ...descendants];
            });
            return children;
        }
    }
    addSuiteResult(debugSession, result) {
        var _a;
        (_a = this.suiteResults.get(debugSession)) === null || _a === void 0 ? void 0 : _a.push(result);
    }
    getSuiteResultsFor(debugSession) {
        return this.suiteResults.get(debugSession);
    }
    setEmptySuiteResultsFor(debugSession) {
        this.suiteResults.set(debugSession, []);
    }
    clearSuiteResultsFor(debugSession) {
        this.suiteResults.delete(debugSession);
    }
}
exports.testCache = new TestCache();
//# sourceMappingURL=test-cache.js.map