"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.runHandler = exports.testRunnerId = void 0;
const metals_languageclient_1 = require("metals-languageclient");
const vscode = __importStar(require("vscode"));
const vscode_1 = require("vscode");
const scalaDebugger_1 = require("../scalaDebugger");
const analyze_test_run_1 = require("./analyze-test-run");
const test_cache_1 = require("./test-cache");
// this id is used to mark DAP sessions created by TestController
// thanks to that debug tracker knows which requests it should track and gather results
exports.testRunnerId = "scala-dap-test-runner";
/**
 * Register tracker which tracks all DAP sessions which are started with @constant {testRunnerId} kind.
 * Dap sends execution result for every suite included in TestRun in a special event of kind 'testResult'.
 * Tracker has to capture these events and store them all in map under debug session id as a key.
 */
vscode.debug.registerDebugAdapterTrackerFactory("scala", {
    createDebugAdapterTracker(session) {
        if (session.configuration.kind === exports.testRunnerId) {
            return {
                onWillStartSession: () => test_cache_1.testCache.setEmptySuiteResultsFor(session.id),
                onDidSendMessage: (msg) => {
                    if (msg.event === "testResult" &&
                        msg.body.category === "testResult") {
                        test_cache_1.testCache.addSuiteResult(session.id, msg.body.data);
                    }
                },
            };
        }
    },
});
/**
 * runHandler is a function which is called to start a TestRun. Depending on the run profile it may be ordinary run or a debug TestRun.
 * It creates run queue which contains test supposed to be run and then for each entry it creates & run a debug session
 */
function runHandler(testController, noDebug, callback, request, token) {
    return __awaiter(this, void 0, void 0, function* () {
        const run = testController.createTestRun(request);
        const queue = createRunQueue(request);
        for (const { test, data } of queue) {
            if (token.isCancellationRequested) {
                run.skipped(test);
            }
            else {
                run.started(test);
                const children = test_cache_1.testCache.getTestItemChildren(test);
                children.forEach((c) => run.enqueued(c));
                try {
                    yield vscode_1.commands.executeCommand("workbench.action.files.save");
                    const testsIds = children.map((t) => t.id);
                    const session = yield createDebugSession(data.targetUri, testsIds);
                    if (!session) {
                        return;
                    }
                    const wasStarted = yield startDebugging(session, noDebug);
                    if (!wasStarted) {
                        vscode.window.showErrorMessage("Debug session not started");
                        return run.failed(test, { message: "Debug session not started" });
                    }
                    yield analyzeResults(run, children, callback);
                }
                catch (error) {
                    console.error(error);
                }
            }
        }
        run.end();
    });
}
exports.runHandler = runHandler;
/**
 * Loop through all included tests in request and add them to our queue if they are not excluded explicitly
 */
function createRunQueue(request) {
    var _a;
    const queue = [];
    if (request.include) {
        const excludes = new Set((_a = request.exclude) !== null && _a !== void 0 ? _a : []);
        for (const test of request.include) {
            if (!excludes.has(test)) {
                const testData = test_cache_1.testCache.getMetadata(test);
                if (testData != null) {
                    queue.push({ test, data: testData });
                }
            }
        }
    }
    return queue;
}
/**
 * Creates a debug session via Metals DebugAdapterStart command.
 * dataKind and data are determined by BSP debug request method.
 */
function createDebugSession(targetUri, testsIds) {
    return __awaiter(this, void 0, void 0, function* () {
        return vscode.commands.executeCommand(metals_languageclient_1.ServerCommands.DebugAdapterStart, {
            targets: [{ uri: targetUri }],
            dataKind: "scala-test-suites",
            data: testsIds,
        });
    });
}
/**
 * Starts interacting with created debug session.
 * kind is set to the testRunnerId. It helps to differentiate debug session which were started by Test Explorer.
 * These sessions are tracked by a vscode.debug.registerDebugAdapterTrackerFactory, which capture special event
 * containing information about test suite execution.
 */
function startDebugging(session, noDebug) {
    return __awaiter(this, void 0, void 0, function* () {
        const port = (0, scalaDebugger_1.debugServerFromUri)(session.uri).port;
        const configuration = {
            type: "scala",
            name: session.name,
            noDebug,
            request: "launch",
            debugServer: port,
            kind: exports.testRunnerId,
        };
        return vscode.debug.startDebugging(undefined, configuration);
    });
}
/**
 * Analyze test results when debug session ends.
 * Retrieves test suite results for current debus session gathered by DAP tracker and passes
 * them to the analyzer function. After analysis ends, results are cleaned.
 */
function analyzeResults(run, children, callback) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve) => {
            const disposable = vscode.debug.onDidTerminateDebugSession((session) => {
                var _a;
                const testSuitesResult = (_a = test_cache_1.testCache.getSuiteResultsFor(session.id)) !== null && _a !== void 0 ? _a : [];
                // disposes current subscription and removes data from result map
                const teardown = () => {
                    disposable.dispose();
                    test_cache_1.testCache.clearSuiteResultsFor(session.id);
                    callback();
                };
                // analyze current TestRun
                (0, analyze_test_run_1.analyzeTestRun)(run, children, testSuitesResult, teardown);
                return resolve();
            });
        });
    });
}
//# sourceMappingURL=test-run-handler.js.map