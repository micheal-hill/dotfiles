"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.analyzeTestRun = void 0;
const ansicolor_1 = require("ansicolor");
/**
 * Analyze results from TestRun and pass inform Test Controller about them.
 *
 * @param run Interface which corresponds to available actions in vscode.TestRun
 * @param tests which should have been run in this TestRun
 * @param testSuitesResult result which came back from DAP server
 * @param teardown cleanup logic which has to be called at the end of function
 */
const analyzeTestRun = (run, tests, testSuitesResults, teardown) => {
    var _a, _b, _c;
    const results = createResultsMap(testSuitesResults);
    for (const test of tests) {
        const suiteName = test.id;
        const result = results.get(suiteName);
        if (result != null) {
            const duration = result.duration;
            const failed = result.tests.filter(isFailed);
            if (failed.length > 0) {
                const msg = extractErrorMessages(failed);
                (_a = run.failed) === null || _a === void 0 ? void 0 : _a.call(run, test, msg, duration);
            }
            else {
                (_b = run.passed) === null || _b === void 0 ? void 0 : _b.call(run, test, duration);
            }
        }
        else {
            (_c = run.skipped) === null || _c === void 0 ? void 0 : _c.call(run, test);
        }
    }
    teardown === null || teardown === void 0 ? void 0 : teardown();
};
exports.analyzeTestRun = analyzeTestRun;
function isFailed(result) {
    return result.kind === "failed";
}
/**
 * Transforms array of suite results into mapping between suite name and suite result
 */
function createResultsMap(testSuitesResults) {
    const resultsTuples = testSuitesResults.map((result) => [result.suiteName, result]);
    const results = new Map(resultsTuples);
    return results;
}
/**
 * Extract error messages for array of failed tests and map them to the TestMessage.
 * Messages can include ANSI escape sequences such as colors, but they have to be stripped
 * because vscode test explorer doesn't support ANSI color codes.
 */
function extractErrorMessages(failed) {
    const msg = failed
        .map((t) => ansicolor_1.ansicolor.strip(t.error))
        .map((message) => ({ message }));
    return msg;
}
//# sourceMappingURL=analyze-test-run.js.map