"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTestManager = void 0;
const vscode = __importStar(require("vscode"));
const vscode_1 = require("vscode");
const node_1 = require("vscode-languageclient/node");
const test_cache_1 = require("./test-cache");
const test_run_handler_1 = require("./test-run-handler");
const util_1 = require("./util");
function createTestManager(client, isDisabled) {
    return new TestManager(client, isDisabled);
}
exports.createTestManager = createTestManager;
class TestManager {
    constructor(client, isDisabled) {
        this.client = client;
        this.testController = vscode_1.tests.createTestController("metalsTestController", "Metals Test Explorer");
        this.isDisabled = false;
        this.isRunning = false;
        if (isDisabled) {
            this.disable();
        }
        this.testController.resolveHandler = (item) => __awaiter(this, void 0, void 0, function* () {
            if (item != null) {
                return;
            }
            else {
                yield this.discoverTestSuites();
            }
        });
        const callback = () => (this.isRunning = false);
        this.testController.createRunProfile("Run", vscode_1.TestRunProfileKind.Run, (request, token) => {
            if (!this.isRunning) {
                this.isRunning = true;
                (0, test_run_handler_1.runHandler)(this.testController, true, callback, request, token);
            }
        }, true);
        this.testController.createRunProfile("Debug", vscode_1.TestRunProfileKind.Debug, (request, token) => {
            if (!this.isRunning) {
                this.isRunning = true;
                (0, test_run_handler_1.runHandler)(this.testController, false, callback, request, token);
            }
        }, false);
    }
    enable() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isDisabled = false;
            yield this.discoverTestSuites();
        });
    }
    /**
     * Disables test manager, also it deletes all discovered test items in order to hide gutter icons.
     */
    disable() {
        this.testController.items.forEach((item) => this.testController.items.delete(item.id));
        this.isDisabled = true;
    }
    discoverTestSuites() {
        if (this.isDisabled) {
            return Promise.resolve();
        }
        return this.client
            .sendRequest(node_1.ExecuteCommandRequest.type, {
            command: "discover-test-suites",
        })
            .then((value) => {
            for (const { targetName, targetUri, discovered } of value) {
                const rootNode = this.testController.createTestItem(targetName, targetName);
                createTestItems(this.testController, discovered, rootNode, targetName, targetUri);
                const data = {
                    kind: "project",
                    targetName,
                    targetUri,
                };
                test_cache_1.testCache.setMetadata(rootNode, data);
                this.testController.items.add(rootNode);
            }
        }, (err) => console.error(err));
    }
}
/**
 * Create TestItems from the given @param discoveredArray and add them to the @param parent
 */
function createTestItems(testController, discoveredArray, parent, targetName, targetUri) {
    for (const discovered of discoveredArray) {
        if (discovered.kind === "suite") {
            const { className, location, fullyQualifiedName } = discovered;
            const parsedUri = vscode.Uri.parse(location.uri);
            const parsedRange = (0, util_1.toVscodeRange)(location.range);
            const testItem = testController.createTestItem(fullyQualifiedName, className, parsedUri);
            testItem.range = parsedRange;
            const data = {
                kind: "suite",
                targetName,
                targetUri,
            };
            test_cache_1.testCache.setMetadata(testItem, data);
            parent.children.add(testItem);
        }
        else {
            const data = {
                kind: "package",
                targetName,
                targetUri,
            };
            const packageNode = testController.createTestItem(`${parent.id}.${discovered.prefix}`, discovered.prefix);
            parent.children.add(packageNode);
            test_cache_1.testCache.setMetadata(packageNode, data);
            createTestItems(testController, discovered.children, packageNode, targetName, targetUri);
        }
    }
}
//# sourceMappingURL=test-manager.js.map