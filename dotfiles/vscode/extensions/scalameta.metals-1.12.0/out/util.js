"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getValueFromConfig = exports.executeCommand = exports.getTextDocumentPositionParams = void 0;
const vscode_languageclient_1 = require("vscode-languageclient");
function getTextDocumentPositionParams(editor) {
    const pos = editor.selection.active;
    return {
        textDocument: { uri: editor.document.uri.toString() },
        position: { line: pos.line, character: pos.character },
    };
}
exports.getTextDocumentPositionParams = getTextDocumentPositionParams;
function executeCommand(client, command, ...args) {
    return client.sendRequest(vscode_languageclient_1.ExecuteCommandRequest.type, {
        command,
        arguments: args,
    });
}
exports.executeCommand = executeCommand;
function getValueFromConfig(config, key, defaultValue) {
    const inspected = config.inspect(key);
    const fromConfig = (inspected === null || inspected === void 0 ? void 0 : inspected.workspaceValue) ||
        (inspected === null || inspected === void 0 ? void 0 : inspected.globalValue) ||
        (inspected === null || inspected === void 0 ? void 0 : inspected.defaultValue);
    return fromConfig !== null && fromConfig !== void 0 ? fromConfig : defaultValue;
}
exports.getValueFromConfig = getValueFromConfig;
//# sourceMappingURL=util.js.map