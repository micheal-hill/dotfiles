"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fse = require("fs-extra-plus");
let snippets = {};
function libsToSnippets(path, builtin) {
    return __awaiter(this, void 0, void 0, function* () {
        let libs = yield fse.listDirs(path);
        yield Promise.all(libs.map((lib) => __awaiter(this, void 0, void 0, function* () {
            let preds = yield fse.listFiles(lib, ".txt");
            yield Promise.all(preds.map((pred) => __awaiter(this, void 0, void 0, function* () {
                let snippet = yield fileToSnippet(pred);
                if (snippet) {
                    let key = pred
                        .split("/")
                        .slice(-2)
                        .join(":")
                        .replace("-", "/")
                        .replace(/\.txt$/, "");
                    if (builtin) {
                        key = key.split(":")[1];
                    }
                    snippets[key] = snippet;
                }
            })));
        })));
    });
}
function fileToSnippet(file) {
    return __awaiter(this, void 0, void 0, function* () {
        if (/summary\.txt$/.test(file)) {
            return null;
        }
        try {
            let snippet = null;
            let txt = yield fse.readFile(file, "utf8");
            let str = txt
                .toString()
                .replace(/\n\n+/g, "\n\n")
                .replace(/ {8,}/g, "    ")
                .trim();
            let match = str.match(/^(\w+)(\(([^\)]*)\))?/);
            let prefix = "", params = "", body;
            if (match) {
                prefix = match[1];
                body = prefix;
                if (match[2]) {
                    params = match[3];
                    let plist = params.split(",");
                    body += "(";
                    for (let i = 1; i <= plist.length; i++) {
                        let mtch = plist[i - 1].match(/\w+/);
                        if (mtch) {
                            let pName = mtch[0];
                            body += "${" + i + ":" + pName + "}";
                            if (i < plist.length) {
                                body += ", ";
                            }
                            else {
                                body += ")$" + (i + 1) + "\n$0";
                            }
                        }
                    }
                }
                snippet = {
                    prefix: prefix,
                    body: body,
                    description: str
                };
            }
            return snippet;
        }
        catch (error) {
            console.log(error);
            return undefined;
        }
    });
}
(() => __awaiter(this, void 0, void 0, function* () {
    let docRoot = "/opt/eclipseclp/doc/bips/";
    yield libsToSnippets(docRoot + "kernel", true);
    yield libsToSnippets(docRoot + "lib", false);
    yield libsToSnippets(docRoot + "lib_public", false);
    yield fse.writeJson("prolog.ecl.json", snippets);
}))();
//# sourceMappingURL=snippets_from_txt.ecl.js.map