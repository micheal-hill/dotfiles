"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode_1 = require("vscode");
const path = require("path");
const prologTerminal_1 = require("./features/prologTerminal");
const editHelpers_1 = require("./features/editHelpers");
const utils_1 = require("./utils/utils");
const hoverProvider_1 = require("./features/hoverProvider");
const documentHighlightProvider_1 = require("./features/documentHighlightProvider");
const formattingEditProvider_1 = require("./features/formattingEditProvider");
const definitionProvider_1 = require("./features/definitionProvider");
const referenceProvider_1 = require("./features/referenceProvider");
const prologLinter_1 = require("./features/prologLinter");
const prologRefactor_1 = require("./features/prologRefactor");
const fs_extra_plus_1 = require("fs-extra-plus");
const jsesc = require("jsesc");
const fs = require("fs");
function initForDialect(context) {
    return __awaiter(this, void 0, void 0, function* () {
        const section = vscode_1.workspace.getConfiguration("prolog");
        const dialect = section.get("dialect");
        const exec = section.get("executablePath", "swipl");
        utils_1.Utils.LINTERTRIGGER = section.get("linter.run");
        utils_1.Utils.FORMATENABLED = section.get("format.enabled");
        utils_1.Utils.DIALECT = dialect;
        utils_1.Utils.RUNTIMEPATH = jsesc(exec);
        const exPath = jsesc(context.extensionPath);
        const diaFile = path.resolve(`${exPath}/.vscode`) + "/dialect.json";
        const lastDialect = JSON.parse(fs.readFileSync(diaFile).toString()).dialect;
        if (lastDialect === dialect) {
            return;
        }
        const symLinks = [
            {
                path: path.resolve(`${exPath}/syntaxes`),
                srcFile: `prolog.${dialect}.tmLanguage.json`,
                targetFile: "prolog.tmLanguage.json"
            },
            {
                path: path.resolve(`${exPath}/snippets`),
                srcFile: `prolog.${dialect}.json`,
                targetFile: "prolog.json"
            }
        ];
        yield Promise.all(symLinks.map((link) => __awaiter(this, void 0, void 0, function* () {
            yield fs_extra_plus_1.remove(path.resolve(`${link.path}/${link.targetFile}`));
            try {
                return yield fs_extra_plus_1.ensureSymlink(path.resolve(`${link.path}/${link.srcFile}`), path.resolve(`${link.path}/${link.targetFile}`));
            }
            catch (err) {
                vscode_1.window.showErrorMessage("VSC-Prolog failed in initialization. Try to run vscode in administrator role.");
                throw (err);
            }
        })));
        fs.writeFileSync(diaFile, JSON.stringify({ dialect: dialect }));
    });
}
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log('Congratulations, your extension "vsc-prolog" is now active!');
        yield initForDialect(context);
        const PROLOG_MODE = { language: "prolog", scheme: "file" };
        utils_1.Utils.init(context);
        editHelpers_1.loadEditHelpers(context.subscriptions);
        let myCommands = [
            {
                command: "prolog.load.document",
                callback: () => {
                    prologTerminal_1.default.loadDocument();
                }
            },
            {
                command: "prolog.query.goal",
                callback: () => {
                    prologTerminal_1.default.queryGoalUnderCursor();
                }
            },
            {
                command: "prolog.refactorPredicate",
                callback: () => {
                    new prologRefactor_1.PrologRefactor().refactorPredUnderCursor();
                }
            }
        ];
        let linter;
        if (utils_1.Utils.LINTERTRIGGER !== "never") {
            linter = new prologLinter_1.default(context);
            linter.activate();
            myCommands = myCommands.concat([
                {
                    command: "prolog.linter.nextErrLine",
                    callback: () => {
                        linter.nextErrLine();
                    }
                },
                {
                    command: "prolog.linter.prevErrLine",
                    callback: () => {
                        linter.prevErrLine();
                    }
                }
            ]);
        }
        myCommands.map(command => {
            context.subscriptions.push(vscode_1.commands.registerCommand(command.command, command.callback));
        });
        if (utils_1.Utils.LINTERTRIGGER !== "never") {
            context.subscriptions.push(vscode_1.languages.registerCodeActionsProvider(PROLOG_MODE, linter));
        }
        context.subscriptions.push(vscode_1.languages.registerHoverProvider(PROLOG_MODE, new hoverProvider_1.default()));
        context.subscriptions.push(vscode_1.languages.registerDocumentHighlightProvider(PROLOG_MODE, new documentHighlightProvider_1.default()));
        if (process.platform !== "win32" && utils_1.Utils.FORMATENABLED) {
            context.subscriptions.push(vscode_1.languages.registerDocumentRangeFormattingEditProvider(PROLOG_MODE, new formattingEditProvider_1.default()));
            context.subscriptions.push(vscode_1.languages.registerOnTypeFormattingEditProvider(PROLOG_MODE, new formattingEditProvider_1.default(), ".", "\n"));
            context.subscriptions.push(vscode_1.languages.registerDocumentFormattingEditProvider(PROLOG_MODE, new formattingEditProvider_1.default()));
        }
        context.subscriptions.push(vscode_1.languages.registerDefinitionProvider(PROLOG_MODE, new definitionProvider_1.PrologDefinitionProvider()));
        context.subscriptions.push(vscode_1.languages.registerReferenceProvider(PROLOG_MODE, new referenceProvider_1.PrologReferenceProvider()));
        context.subscriptions.push(prologTerminal_1.default.init());
        // context.subscriptions.push(prologDebugger);
    });
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map