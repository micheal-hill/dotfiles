"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const cp = require("child_process");
const utils_1 = require("../utils/utils");
const path = require("path");
const jsesc = require("jsesc");
class PrologDefinitionProvider {
    provideDefinition(doc, position, token) {
        let location = null;
        let pred = utils_1.Utils.getPredicateUnderCursor(doc, position);
        if (!pred) {
            return null;
        }
        let exec = utils_1.Utils.RUNTIMEPATH;
        let args = [], prologCode, result, predToFind, runOptions;
        const fileLineRe = /File:(.+);Line:(\d+)/;
        switch (utils_1.Utils.DIALECT) {
            case "swi":
                args = ["-q", doc.fileName];
                prologCode = `
        source_location:-
          (current_predicate(${pred.pi}) ->
            Pred = ${pred.wholePred}
          ; DcgArity is ${pred.arity + 2},
            functor(Term, ${pred.functor}, DcgArity),
            Pred = ${pred.module}:Term
          ),
          predicate_property(Pred, file(File)),
          predicate_property(Pred, line_count(Line)),
          format("File:~s;Line:~d~n", [File, Line]).
          `;
                if (doc.isDirty) {
                    doc.save().then(_ => {
                        result = utils_1.Utils.execPrologSync(args, prologCode, "source_location", "", fileLineRe);
                    });
                }
                else {
                    result = utils_1.Utils.execPrologSync(args, prologCode, "source_location", "", fileLineRe);
                }
                break;
            case "ecl":
                args = [];
                let lc = path.resolve(`${__dirname}/locate_clause`);
                predToFind = pred.pi.split(":")[1];
                prologCode = `ensure_loaded(['${lc}']),
          source_location('${jsesc(doc.fileName)}', ${predToFind}).
          `;
                runOptions = {
                    cwd: vscode_1.workspace.rootPath,
                    encoding: "utf8",
                    input: prologCode
                };
                if (doc.isDirty) {
                    doc.save().then(_ => {
                        let syncPro = cp.spawnSync(utils_1.Utils.RUNTIMEPATH, args, runOptions);
                        if (syncPro.status === 0) {
                            result = syncPro.stdout.toString().match(fileLineRe);
                        }
                    });
                }
                else {
                    let syncPro = cp.spawnSync(utils_1.Utils.RUNTIMEPATH, args, runOptions);
                    if (syncPro.status === 0) {
                        result = syncPro.stdout.toString().match(fileLineRe);
                    }
                }
                break;
            default:
                break;
        }
        if (result) {
            let fileName = result[1];
            let lineNum = parseInt(result[2]);
            location = new vscode_1.Location(vscode_1.Uri.file(fileName), new vscode_1.Position(lineNum - 1, 0));
        }
        return location;
    }
}
exports.PrologDefinitionProvider = PrologDefinitionProvider;
//# sourceMappingURL=definitionProvider.js.map