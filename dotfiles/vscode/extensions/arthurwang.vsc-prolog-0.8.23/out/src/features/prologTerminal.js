"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils/utils");
const vscode_1 = require("vscode");
const jsesc = require("jsesc");
class PrologTerminal {
    constructor() { }
    static init() {
        return vscode_1.window.onDidCloseTerminal(terminal => {
            PrologTerminal._terminal = null;
            terminal.dispose();
        });
    }
    static createPrologTerm() {
        if (PrologTerminal._terminal) {
            return;
        }
        let section = vscode_1.workspace.getConfiguration("prolog");
        let title = "Prolog";
        if (section) {
            let executable = section.get("executablePath", "swipl");
            let args = section.get("terminal.runtimeArgs");
            PrologTerminal._terminal = vscode_1.window.createTerminal(title, executable, args);
        }
        else {
            throw new Error("configuration settings error: prolog");
        }
    }
    static sendString(text) {
        PrologTerminal.createPrologTerm();
        if (!text.endsWith(".")) {
            text += ".";
        }
        PrologTerminal._terminal.sendText(text);
        PrologTerminal._terminal.show(false);
    }
    static loadDocument() {
        PrologTerminal._document = vscode_1.window.activeTextEditor.document;
        PrologTerminal.createPrologTerm();
        let fname = jsesc(PrologTerminal._document.fileName, { quotes: "single" });
        let goals = `['${fname}']`;
        if (PrologTerminal._document.isDirty) {
            PrologTerminal._document.save().then(_ => {
                PrologTerminal.sendString(goals);
            });
        }
        else {
            PrologTerminal.sendString(goals);
        }
    }
    static queryGoalUnderCursor() {
        let editor = vscode_1.window.activeTextEditor;
        let doc = editor.document;
        let pred = utils_1.Utils.getPredicateUnderCursor(doc, editor.selection.active);
        if (!pred) {
            return;
        }
        PrologTerminal.loadDocument();
        let goal = pred.wholePred;
        if (goal.indexOf(":") > -1) {
            goal = goal.split(":")[1];
        }
        PrologTerminal.sendString(goal);
    }
}
exports.default = PrologTerminal;
//# sourceMappingURL=prologTerminal.js.map