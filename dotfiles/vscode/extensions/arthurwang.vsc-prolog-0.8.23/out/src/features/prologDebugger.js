"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const fs = require("fs");
const process_promises_1 = require("process-promises");
const prologDebugSession_1 = require("./prologDebugSession");
const vscode_debugadapter_1 = require("vscode-debugadapter");
const path_1 = require("path");
// import * as Net from "net";
const jsesc = require("jsesc");
class PrologDebugger extends events_1.EventEmitter {
    // private _client: Net.Socket = null;
    constructor(launchRequestArguments, debugSession) {
        super();
        this._prologProc = null;
        this._launchRequestArguments = launchRequestArguments;
        this._debugSession = debugSession;
        this._soureLineLocations = {};
        // this._client = client || null;
        this.createPrologProc();
        console.log("prolog debugger constructed");
    }
    getSourceLineLocations(source) {
        if (this._soureLineLocations[source]) {
            return;
        }
        let lines = fs
            .readFileSync(source)
            .toString()
            .split("\n");
        let lengths = lines.map(line => {
            return line.length + 1;
        });
        lengths.unshift(0);
        for (let i = 1; i < lengths.length; i++) {
            lengths[i] += lengths[i - 1];
        }
        this._soureLineLocations[source] = lengths;
    }
    fromStartCharToLineChar(source, startChar) {
        this.getSourceLineLocations(source);
        let i = 0;
        for (; this._soureLineLocations[source][i] < startChar; i++)
            ;
        return {
            file: source,
            line: i + 1,
            startChar: startChar - this._soureLineLocations[source][i]
        };
    }
    handleOutput(data) {
        let resObj;
        try {
            resObj = JSON.parse(data);
            if (Object.keys(resObj)[0] !== "response") {
                return;
            }
        }
        catch (error) {
            return;
        }
        switch (Object.keys(resObj.response)[0]) {
            case "breakpoints":
                this._bpResponse.body = {
                    breakpoints: resObj.response.breakpoints
                };
                this.emit("responseBreakpoints", this._bpResponse);
                return;
            case "functionbps":
                this._fbpResponse.body = {
                    breakpoints: resObj.response.functionbps
                };
                this.emit("responseFunctionBreakpoints", this._fbpResponse);
                return;
            case "frame":
                let frame = resObj.response.frame;
                this._debugSession.addStackFrame(frame);
                this._debugSession.sendEvent(new vscode_debugadapter_1.StoppedEvent(frame.name, prologDebugSession_1.PrologDebugSession.THREAD_ID));
                return;
            case "variables":
                this._debugSession.setCurrentVariables(resObj.response.variables);
                return;
            default:
                break;
        }
    }
    query(goal) {
        if (!/^\n$/.test(goal)) {
            goal = goal.replace(/\n+/g, "\n");
            let from = goal.indexOf(":");
            if (from < 0) {
                return;
            }
            this._prologProc.stdin.write(goal.substr(from + 1));
        }
    }
    killPrologProc() {
        if (this._prologProc) {
            this._prologProc.kill();
        }
    }
    filterOffOutput(data) {
        const regs = [
            /^$/,
            /^TermToBeEvaluated/,
            /^EvalTermAtom/,
            /^EvalVarNames/,
            /^E =/,
            /^true\./
        ];
        for (let i = 0; i < regs.length; i++) {
            if (regs[i].test(data)) {
                return true;
            }
        }
        return false;
    }
    get pid() {
        return this._prologProc.pid;
    }
    initPrologDebugger() {
        //   let dirName = jsesc(__dirname);
        let dbg = jsesc(path_1.resolve(`${__dirname}/debugger`));
        console.log(dbg);
        this._prologProc.stdin.write(`
          use_module('${dbg}').\n
          prolog_debugger:load_source_file('${jsesc(this._launchRequestArguments.program)}').
            `);
    }
    createPrologProc() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("path:" + this._launchRequestArguments.runtimeExecutable);
            this.killPrologProc();
            let pp = yield process_promises_1.spawn(jsesc(this._launchRequestArguments.runtimeExecutable), this._launchRequestArguments.runtimeArgs.concat("-q"), { cwd: this._launchRequestArguments.cwd })
                .on("process", proc => {
                if (proc.pid) {
                    this._prologProc = proc;
                    this.initPrologDebugger();
                }
            })
                .on("stdout", data => {
                //this._debugSession.debugOutput("\n" + data);
                if (/"response":/.test(data)) {
                    this.handleOutput(data);
                }
                else if (!this.filterOffOutput(data)) {
                    this._debugSession.debugOutput("\n" + data);
                }
            })
                .on("stderr", err => {
                //this._debugSession.debugOutput("\n" + err);
                this._debugSession.sendEvent(new vscode_debugadapter_1.OutputEvent(err + "\n", "stderr"));
            })
                .on("exit", () => {
                this._debugSession.sendEvent(new vscode_debugadapter_1.TerminatedEvent());
            })
                .then(result => {
                this._debugSession.debugOutput("\nProlog process exit with code:" + result.exitCode);
            })
                .catch(error => {
                let message = null;
                if (error.code === "ENOENT") {
                    message = `Cannot debug the prolog file. The Prolog executable '${this._launchRequestArguments.runtimeExecutable}' was not found. Correct 'runtimeExecutable' setting in launch.json file.`;
                }
                else {
                    message = error.message
                        ? error.message
                        : `Failed to run swipl using path: ${this._launchRequestArguments.runtimeExecutable}. Reason is unknown.`;
                }
                this._debugSession.debugOutput("\n" + message);
                throw new Error(error);
            });
        });
    }
    consult() {
        let fileName = this._launchRequestArguments.program;
        let goals = "['" + fileName + "'].\n";
        this._prologProc.stdin.write(goals);
    }
    setBreakpoints(breakpoints, bpResponse) {
        this._bpResponse = bpResponse;
        let path = jsesc(path_1.resolve(breakpoints.source.path));
        let bps = breakpoints.breakpoints.map(bp => {
            return JSON.stringify({
                line: bp.line,
                column: bp.column,
                condition: bp.condition,
                hitCondition: bp.hitCondition
            });
        });
        let cmd = `cmd:prolog_debugger:set_breakpoints('${path}', ${JSON.stringify(bps.join(";"))}).\n`;
        this.query(cmd);
    }
    setFunctionBreakpoints(args, response) {
        let preds = args.breakpoints.map(bp => {
            return bp.name;
        });
        this._fbpResponse = response;
        let cmd = `cmd:prolog_debugger:spy_predicates([${preds}]).\n`;
        this.query(cmd);
    }
    startup(goal) {
        let cmd = `cmd:prolog_debugger:startup(${goal}).\n`;
        this.query(cmd);
    }
    dispose() {
        this.killPrologProc();
    }
}
exports.PrologDebugger = PrologDebugger;
//# sourceMappingURL=prologDebugger.js.map