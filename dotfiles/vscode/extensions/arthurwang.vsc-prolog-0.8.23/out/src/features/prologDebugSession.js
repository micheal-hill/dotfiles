"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_debugadapter_1 = require("vscode-debugadapter");
const prologDebugger_1 = require("./prologDebugger");
const path = require("path");
const process_promises_1 = require("process-promises");
class PrologDebugSession extends vscode_debugadapter_1.DebugSession {
    constructor() {
        super();
        this._currentVariables = [];
        this._stackFrames = [];
        this.setDebuggerColumnsStartAt1(true);
        this.setDebuggerLinesStartAt1(true);
        this.setDebuggerPathFormat("native");
    }
    initializeRequest(response, args) {
        response.body = {
            supportsConfigurationDoneRequest: true,
            supportTerminateDebuggee: true,
            supportsConditionalBreakpoints: true,
            supportsHitConditionalBreakpoints: true,
            supportsFunctionBreakpoints: true,
            supportsEvaluateForHovers: true,
            supportsExceptionOptions: true,
            supportsExceptionInfoRequest: true,
            exceptionBreakpointFilters: [
                {
                    filter: "Notice",
                    label: "Notices"
                },
                {
                    filter: "Warning",
                    label: "Warnings"
                },
                {
                    filter: "Error",
                    label: "Errors"
                },
                {
                    filter: "Exception",
                    label: "Exceptions"
                },
                {
                    filter: "*",
                    label: "Everything",
                    default: true
                }
            ]
        };
        this.sendResponse(response);
    }
    attachRequest(response, args) {
        this.sendErrorResponse(response, new Error("Attach requests are not supported"));
        this.shutdown();
    }
    addStackFrame(frame) {
        this._stackFrames.unshift(new vscode_debugadapter_1.StackFrame(frame.id, `(${frame.level})${frame.name}`, new vscode_debugadapter_1.Source(path.basename(frame.file), this.convertDebuggerPathToClient(frame.file)), this.convertDebuggerLineToClient(frame.line), this.convertDebuggerColumnToClient(frame.column)));
    }
    setCurrentVariables(vars) {
        this._currentVariables = [];
        while (vars.length > 0) {
            this._currentVariables.push(vars.pop());
        }
    }
    launchRequest(response, args) {
        return __awaiter(this, void 0, void 0, function* () {
            // window.showInformationMessage("hello");
            this._startupQuery = args.startupQuery || "start";
            this._startFile = path.resolve(args.program);
            this._cwd = args.cwd;
            this._runtimeExecutable = args.runtimeExecutable || "swipl";
            // this._runtimeArgs = args.runtimeArgs || null;
            this._stopOnEntry = typeof args.stopOnEntry ? args.stopOnEntry : true;
            this._traceCmds = args.traceCmds;
            this._prologDebugger = yield new prologDebugger_1.PrologDebugger(args, this);
            this._prologDebugger.addListener("responseBreakpoints", (bps) => {
                this.sendResponse(bps);
            });
            this._prologDebugger.addListener("responseFunctionBreakpoints", (fbps) => {
                this.sendResponse(fbps);
            });
            this.sendResponse(response);
            this.sendEvent(new vscode_debugadapter_1.InitializedEvent());
        });
    }
    threadsRequest(response) {
        response.body = {
            threads: [new vscode_debugadapter_1.Thread(PrologDebugSession.THREAD_ID, "thread 1")]
        };
        this.sendResponse(response);
    }
    setBreakPointsRequest(response, args) {
        if (this._debugging) {
            this.debugOutput("Breakpoints set during debugging would take effect in next debugging process.");
            return;
        }
        this._prologDebugger.setBreakpoints(args, response);
    }
    setExceptionBreakPointsRequest(response, args) {
        this.sendResponse(response);
    }
    setFunctionBreakPointsRequest(response, args) {
        this._prologDebugger.setFunctionBreakpoints(args, response);
    }
    configurationDoneRequest(response, args) {
        this.sendResponse(response);
        this._prologDebugger.startup(`${this._startupQuery}`);
        if (!this._stopOnEntry) {
            this._prologDebugger.query(`cmd:${this._traceCmds.continue[1]}\n`);
        }
        this._debugging = true;
    }
    evaluateExpression(exp) {
        const vars = this._currentVariables;
        for (let i = 0; i < vars.length; i++) {
            if (vars[i].name === exp) {
                return vars[i].value;
            }
        }
        return null;
    }
    evaluateRequest(response, args) {
        let val = this.evaluateExpression(args.expression);
        if (val) {
            response.body = {
                result: val,
                variablesReference: 0
            };
            this.sendResponse(response);
            return;
        }
        else {
            if (args.context === "repl") {
                const vars = this._currentVariables;
                let exp = args.expression.trim();
                if (exp.startsWith(":")) {
                    //work around for input from stdin
                    let input = "input" + args.expression;
                    this._prologDebugger.query(input + "\n");
                }
                else {
                    for (let i = 0; i < vars.length; i++) {
                        let re = new RegExp("\\b" + vars[i].name + "\\b", "g");
                        exp = exp.replace(re, vars[i].value);
                    }
                    this.debugOutput(args.expression);
                    this.evaluate(exp);
                }
                response.body = { result: "", variablesReference: 0 };
            }
            this.sendResponse(response);
            return;
        }
    }
    stackTraceRequest(response, args) {
        response.body = {
            stackFrames: this._stackFrames
        };
        this.sendResponse(response);
    }
    variablesRequest(response, args) {
        const variables = new Array();
        for (let i = 0; i < this._currentVariables.length; i++) {
            variables.push(this._currentVariables[i]);
        }
        response.body = {
            variables: variables
        };
        this.sendResponse(response);
    }
    scopesRequest(response, args) {
        const scopes = new Array();
        scopes.push(new vscode_debugadapter_1.Scope("Local", PrologDebugSession.SCOPEREF++, false));
        response.body = {
            scopes: scopes
        };
        this.sendResponse(response);
    }
    continueRequest(response, args) {
        this._prologDebugger.query(`cmd:${this._traceCmds.continue[1]}\n`);
        this.sendResponse(response);
    }
    nextRequest(response, args) {
        this._prologDebugger.query(`cmd:${this._traceCmds.stepover[1]}\n`);
        this.sendResponse(response);
    }
    stepInRequest(response, args) {
        this._prologDebugger.query(`cmd:${this._traceCmds.stepinto[1]}\n`);
        this.sendResponse(response);
    }
    stepOutRequest(response, args) {
        this._prologDebugger.query(`cmd:${this._traceCmds.stepout[1]}\n`);
        this.sendResponse(response);
    }
    disconnectRequest(response, args) {
        this._debugging = false;
        this._prologDebugger.dispose();
        this.shutdown();
        this.sendResponse(response);
    }
    restartRequest(response, args) {
        this._debugging = false;
        this._prologDebugger.dispose();
        this.shutdown();
        this.sendResponse(response);
    }
    sendErrorResponse(response) {
        if (arguments[1] instanceof Error) {
            const error = arguments[1];
            const dest = arguments[2];
            let code;
            if (typeof error.code === "number") {
                code = error.code;
            }
            else if (typeof error.errno === "number") {
                code = error.errno;
            }
            else {
                code = 0;
            }
            super.sendErrorResponse(response, code, error.message, dest);
        }
        else {
            super.sendErrorResponse(response, arguments[1], arguments[2], arguments[3], arguments[4]);
        }
    }
    debugOutput(msg) {
        this.sendEvent(new vscode_debugadapter_1.OutputEvent(msg));
    }
    evaluate(expression) {
        let exec = this._runtimeExecutable;
        let args = ["-q", `${this._startFile}`];
        let input = `
    call((${expression})).
    halt.
    `;
        let spawnOptions = {
            cwd: this._cwd
        };
        process_promises_1.spawn(exec, args, spawnOptions)
            .on("process", proc => {
            if (proc.pid) {
                proc.stdin.write(input);
                proc.stdin.end();
            }
        })
            .on("stdout", data => {
            this.debugOutput("\n" + data);
        })
            .on("stderr", err => {
            this.debugOutput("\n" + err);
        })
            .catch(err => {
            this.debugOutput(err.message);
        });
    }
}
PrologDebugSession.SCOPEREF = 1;
PrologDebugSession.THREAD_ID = 100;
exports.PrologDebugSession = PrologDebugSession;
vscode_debugadapter_1.DebugSession.run(PrologDebugSession);
//# sourceMappingURL=prologDebugSession.js.map