"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const cp = require("child_process");
const utils_1 = require("../utils/utils");
class PrologHoverProvider {
    // escape markdown syntax tokens: http://daringfireball.net/projects/markdown/syntax#backslash
    textToMarkedString(text) {
        return text.replace(/[\\`*_{}[\]()#+\-.!]/g, "\\$&");
    }
    provideHover(doc, position, token) {
        let wordRange = doc.getWordRangeAtPosition(position);
        if (!wordRange) {
            return;
        }
        let pred = utils_1.Utils.getPredicateUnderCursor(doc, position);
        if (!pred) {
            return;
        }
        if (pred.arity < 0) {
            return;
        }
        let contents = [];
        switch (utils_1.Utils.DIALECT) {
            case "swi":
                let pi = pred.pi.indexOf(":") > -1 ? pred.pi.split(":")[1] : pred.pi;
                let modules = utils_1.Utils.getPredModules(pi);
                if (modules.length === 0) {
                    let desc = utils_1.Utils.getPredDescriptions(pi);
                    contents.push({ language: "prolog", value: desc });
                }
                else {
                    if (modules.length > 0) {
                        modules.forEach(module => {
                            contents.push(module + ":" + pi + "\n");
                            let desc = utils_1.Utils.getPredDescriptions(module + ":" + pi);
                            contents.push({ language: "prolog", value: desc });
                        });
                    }
                }
                break;
            case "ecl":
                let pro = cp.spawnSync(utils_1.Utils.RUNTIMEPATH, ["-e", `help(${pred.pi})`]);
                if (pro.status === 0) {
                    contents.push({
                        language: "prolog",
                        value: pro.output
                            .toString()
                            .trim()
                            .replace(/^\W*\n/, "")
                            .replace(/\n{3,}/g, "\n\n")
                            .replace(/  +/g, "  ")
                    });
                }
                else {
                    return;
                }
            default:
                break;
        }
        return contents === [] ? null : new vscode_1.Hover(contents, wordRange);
    }
}
exports.default = PrologHoverProvider;
//# sourceMappingURL=hoverProvider.js.map