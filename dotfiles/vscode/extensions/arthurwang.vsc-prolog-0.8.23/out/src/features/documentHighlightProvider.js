"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
class PrologDocumentHighlightProvider {
    provideDocumentHighlights(doc, position, token) {
        let docHilite = [];
        let wordRange = doc.getWordRangeAtPosition(position);
        if (!wordRange) {
            return;
        }
        let symbol = doc.getText(wordRange);
        let symbolLen = symbol.length;
        let line = 0;
        let re = new RegExp("\\b" + symbol + "\\b", "g");
        while (line < doc.lineCount) {
            let lineTxt = doc.lineAt(line).text;
            let match = re.exec(lineTxt);
            while (match) {
                docHilite.push(new vscode_1.DocumentHighlight(new vscode_1.Range(line, match["index"], line, match["index"] + symbolLen)));
                match = re.exec(lineTxt);
            }
            line++;
        }
        return docHilite;
    }
}
exports.default = PrologDocumentHighlightProvider;
//# sourceMappingURL=documentHighlightProvider.js.map