"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const utils_1 = require("../utils/utils");
const process_promises_1 = require("process-promises");
const fif = require("find-in-files");
const fs = require("fs");
const jsesc = require("jsesc");
const path = require("path");
class PrologRefactor {
    // pick predicate at pos in doc
    constructor() {
        this._locations = [];
        this._clauseRefs = null;
        this._isBuiltin = false;
        this._defLocFound = false;
        this._executable = utils_1.Utils.RUNTIMEPATH;
        this._outputChannel = vscode_1.window.createOutputChannel("PrologFormatter");
        this._locations = [];
        this._clauseRefs = {};
    }
    // pick predicate at pos in doc
    refactorPredUnderCursor() {
        let doc = vscode_1.window.activeTextEditor.document;
        let pos = vscode_1.window.activeTextEditor.selection.active;
        let pred = utils_1.Utils.getPredicateUnderCursor(doc, pos);
        this.findFilesAndRefs(pred, true).then(refLocs => {
            if (this._isBuiltin) {
                vscode_1.window
                    .showInformationMessage(`'${pred.pi}' is a builtin predicate, so its definition cannot be refactored. Are you still SURE to refactor its all references?`, "yes", "no")
                    .then(answer => {
                    if (answer !== "yes") {
                        return;
                    }
                    this.applyRefactoring(pred, refLocs);
                });
            }
            else {
                vscode_1.window
                    .showInformationMessage(`'Are you SURE to refactor '${pred.pi}' in all its references and definition? You'd better to commit current stage of the VCS to rollback refactoring if necessary.`, "yes", "no")
                    .then(answer => {
                    if (answer !== "yes") {
                        return;
                    }
                    this.applyRefactoring(pred, refLocs);
                });
            }
        });
    }
    applyRefactoring(pred, refLocs) {
        return __awaiter(this, void 0, void 0, function* () {
            let newPredName = yield vscode_1.window.showInputBox({
                prompt: `Input new predicate name to replace ${pred.functor}`,
                placeHolder: pred.functor,
                ignoreFocusOut: true,
                validateInput: value => {
                    if (/\s/.test(value) && !/^'[^\']+'$/.test(value)) {
                        return "Predicate name must not contain any spaces, tab and new line.";
                    }
                    if (/^[^a-z']/.test(value) ||
                        (/^'[^a-z]/.test(value) && !/'$/.test(value))) {
                        return "Illegal starting letter in predicate name.";
                    }
                    return null;
                }
            });
            yield Promise.all(refLocs.map((refLoc) => __awaiter(this, void 0, void 0, function* () {
                let edit = new vscode_1.WorkspaceEdit();
                edit.replace(refLoc.uri, refLoc.range, newPredName);
                return yield vscode_1.workspace.applyEdit(edit);
            })));
            yield Promise.all(vscode_1.workspace.textDocuments.map((doc) => __awaiter(this, void 0, void 0, function* () {
                return yield doc.save();
            })));
            return;
        });
    }
    findAllRefs() {
        let doc = vscode_1.window.activeTextEditor.document;
        let pos = vscode_1.window.activeTextEditor.selection.active;
        let pred = utils_1.Utils.getPredicateUnderCursor(doc, pos);
        return this.findFilesAndRefs(pred);
    }
    findFilesAndRefs(pred, includingDefLoc = false) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.findFilesAndRefs1(pred, includingDefLoc);
        });
    }
    findFilesAndRefs1(pred, includingDefLoc = false) {
        return __awaiter(this, void 0, void 0, function* () {
            yield Promise.all(vscode_1.workspace.textDocuments.map((doc) => __awaiter(this, void 0, void 0, function* () {
                return yield doc.save();
            })));
            let files = yield fif.find(pred.functor, jsesc(vscode_1.workspace.rootPath), "\\.pl$|\\.ecl$");
            for (let file in files) {
                file = jsesc(file);
                let defLoc = includingDefLoc && !this._defLocFound;
                yield this.loadFileAndFindRefs(pred.pi, file, defLoc);
            }
            return this._locations;
        });
    }
    loadFileAndFindRefs(pi, file, includingDefLoc = false) {
        return __awaiter(this, void 0, void 0, function* () {
            let input, args = [];
            switch (utils_1.Utils.DIALECT) {
                case "swi":
                    let pfile = jsesc(path.resolve(`${__dirname}/findallrefs_swi`));
                    input = `
          use_module('${pfile}').
          load_files('${file}').
          findrefs:findrefs(${pi}, ${includingDefLoc}).
          halt.
        `;
                    args = ["-q"];
                    break;
                case "ecl":
                    let efile = jsesc(path.resolve(`${__dirname}/findallrefs`));
                    args = ["-f", efile];
                    input = `digout_predicate('${file}', ${pi}). `;
                    break;
                default:
                    break;
            }
            try {
                yield process_promises_1.spawn(this._executable, args, { cwd: vscode_1.workspace.rootPath })
                    .on("process", proc => {
                    if (proc.pid) {
                        proc.stdin.write(input);
                        proc.stdin.end();
                    }
                })
                    .on("stdout", output => {
                    // console.log("out:" + output);
                    switch (utils_1.Utils.DIALECT) {
                        case "swi":
                            this.findRefsFromOutputSwi(pi, output);
                            break;
                        case "ecl":
                            this.findRefsFromOutputEcl(file, pi, output);
                        default:
                            break;
                    }
                })
                    .on("stderr", err => {
                    // console.log("err:" + err);
                    this._outputChannel.append(err + "\n");
                    this._outputChannel.show(true);
                });
            }
            catch (error) {
                let message = null;
                if (error.code === "ENOENT") {
                    message = `Cannot debug the prolog file. The Prolog executable was not found. Correct the 'prolog.executablePath' configure please.`;
                }
                else {
                    message = error.message
                        ? error.message
                        : `Failed to run swipl using path: ${this
                            ._executable}. Reason is unknown.`;
                }
            }
        });
    }
    findRefsFromOutputSwi(pi, output) {
        if (/{"reference":"built_in or foreign"}/.test(output)) {
            this._isBuiltin = true;
        }
        if (/{"reference":"definition location found"}/.test(output)) {
            this._defLocFound = true;
        }
        let refReg = /\{"reference":\s*(\{.+?\})\}/g;
        let match = refReg.exec(output);
        while (match) {
            let ref = JSON.parse(match[1]);
            //relocate if ref points to start of the clause
            let lines = fs
                .readFileSync(ref.file)
                .toString()
                .split("\n");
            let predName = pi.split("/")[0];
            if (predName.indexOf(":") > -1) {
                predName = predName.split(":")[1];
            }
            if (!new RegExp("^" + predName).test(lines[ref.line].slice(ref.char))) {
                let clauseStart = ref.line;
                let start = ref.line;
                if (this._clauseRefs[ref.file] &&
                    this._clauseRefs[ref.file][clauseStart]) {
                    start = this._clauseRefs[ref.file][clauseStart] + 1;
                }
                let str = lines.slice(start).join("\n");
                let index = str.indexOf(predName);
                if (index > -1) {
                    str = str.slice(0, index);
                    let strLines = str.split("\n");
                    ref.line = start + strLines.length - 1;
                    ref.char = strLines[strLines.length - 1].length;
                    if (this._clauseRefs[ref.file]) {
                        this._clauseRefs[ref.file][clauseStart] = ref.line;
                    }
                    else {
                        this._clauseRefs[ref.file] = {};
                        this._clauseRefs[ref.file][clauseStart] = ref.line;
                    }
                }
            }
            this._locations.push(new vscode_1.Location(vscode_1.Uri.file(jsesc(path.resolve(ref.file))), new vscode_1.Range(ref.line, ref.char, ref.line, ref.char + predName.length)));
            match = refReg.exec(output);
        }
    }
    findRefsFromOutputEcl(file, pi, output) {
        // console.log("output:" + output);
        let match = output.match(/references:\[(.*)\]/);
        if (!match || match[1] === "") {
            return;
        }
        let predLen = pi.split(":")[1].split("/")[0].length;
        let locs = match[1].split(",");
        vscode_1.workspace.openTextDocument(vscode_1.Uri.file(file)).then(doc => {
            locs.forEach(fromS => {
                const from = parseInt(fromS);
                this._locations.push(new vscode_1.Location(vscode_1.Uri.file(file), new vscode_1.Range(doc.positionAt(from), doc.positionAt(from + predLen))));
            });
        });
    }
}
exports.PrologRefactor = PrologRefactor;
//# sourceMappingURL=prologRefactor.js.map