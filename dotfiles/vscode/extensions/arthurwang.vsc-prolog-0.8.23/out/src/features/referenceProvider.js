"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prologRefactor_1 = require("./prologRefactor");
const utils_1 = require("../utils/utils");
class PrologReferenceProvider {
    constructor() { }
    provideReferences(doc, position, context, token) {
        let pred = utils_1.Utils.getPredicateUnderCursor(doc, position);
        return new prologRefactor_1.PrologRefactor().findFilesAndRefs(pred);
    }
}
exports.PrologReferenceProvider = PrologReferenceProvider;
//# sourceMappingURL=referenceProvider.js.map